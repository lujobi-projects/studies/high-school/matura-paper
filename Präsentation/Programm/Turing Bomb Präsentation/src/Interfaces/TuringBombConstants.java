package Interfaces;

import java.awt.Color;
import java.util.ArrayList;

/**
 * Sammlung von Konstanten
 *
 * @author Luzian
 */
public interface TuringBombConstants {//Idee von Enigma-Programm übernommen

    /**
     * Anzahl Buchsaben im Alphabet
     */
    public static final int DEFAULTCHARNUMBER = 26;

    /**
     * Zahl zur Umwandlung des Char-Werts in A=0, B=1, usw.
     */
    public static final int CHARVALUE = 65;

    /**
     * Maximale Anzahl von verschiednen Walzenlagen
     */
    public static final int MAXWHEELORDERNR = 17576;

    /**
     * MULTIPLICATIONSIGN_VALUE + CHARVALUE = Charwert von:"*".
     */
    public static final int MULTIPLICATIONSIGN_VALUE = -23;

    /**
     * Pfad zu den Ressourcen
     */
    public static final String PATH_RESOURCES = "/resources/";

    /**
     * Pfad zu den letzten Eingaben
     */
    public static final String PATH_LAST_INPUT = "/lastInputs/";

    /**
     * Pfad zu den Rotoren
     */
    public static final String PATH_ROTOR = PATH_RESOURCES + "Rotor_";

    /**
     * Pfad zu den Umkehrwalzen
     */
    public static final String PATH_UKW = PATH_RESOURCES + "UKW_";

    /**
     * Ende einer Text-Datei
     */
    public static final String FILEEXT_TEXT = ".txt";

    /**
     * ganzes ALphabet
     */
    public static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * Alle Zahlen
     */
    public static final String NUMBERS = "0123456789";

    /**
     * Farbe des Hintergrunds
     */
    public static final Color BACKGROUNDCOLOR = new Color(139, 69, 19);

    /**
     * Farbe der Knöpfe
     */
    public static final Color BUTTONCOLOR = new Color(255, 241, 161);
}
