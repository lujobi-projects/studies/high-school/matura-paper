/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turing.bomb.Test;

import java.util.ArrayList;
import java.util.Arrays;
import turing.bomb.RotorPart.AllWheelOrders;
import turing.bomb.RotorPart.Settings;
import turing.bomb.RotorPart.Solution;
import Interfaces.TuringBombConstants;
import static Interfaces.TuringBombConstants.MAXWHEELORDERNR;

/**
 *
 * @author Luzian
 */
public class TestTuringBomb implements TuringBombConstants {

    private final ArrayList<ArrayList<Integer>> loopsTB = new ArrayList<>();

    public TestTuringBomb(ArrayList<Integer>... loops) {
        this.loopsTB.addAll(Arrays.asList(loops));
    }

    public ArrayList<Solution> find(boolean ukwA, boolean ukwB, boolean ukwC) {
        ArrayList<Solution> solutions = new ArrayList();
        if (ukwA) {
            solutions.addAll(findUKWAonly());
        }
        if (ukwB) {
            solutions.addAll(findUKWBonly());
        }
        if (ukwC) {
            solutions.addAll(findUKWConly());
        }
        return solutions;
    }

    private ArrayList<Solution> findUKWAonly() {
        return findPossibilitiesFromTo(0, 60);
    }

    private ArrayList<Solution> findUKWBonly() {
        return findPossibilitiesFromTo(60, 120);
    }

    private ArrayList<Solution> findUKWConly() {
        return findPossibilitiesFromTo(120, 180);
    }

    private ArrayList<Solution> findPossibilitiesFromTo(int start, int end) {

        ArrayList<Solution> solutions = new ArrayList<>();
        AllWheelOrders poss = new AllWheelOrders();

        for (int i = start; i < end; i++) {
            Settings settings = new Settings(poss.getWheelOrder(i));
            for (int z = 0; z < MAXWHEELORDERNR; z++) {
                if (solutionIsTrue(settings)) {
                    solutions.add(new Solution(poss.getWheelOrder(i), i));
                }
                settings.turnDS();
            }
        }
        return solutions;
    }

    private boolean solutionIsTrue(Settings settings) {
        ArrayList<Integer> results = new ArrayList<>();

        for (ArrayList<Integer> loopsTB1 : loopsTB) {
            int result = settings.input(loopsTB1.get(0), 0);// Problematisch
            for (int i = 1; i < loopsTB.size(); i++) {
                result = settings.input(loopsTB.get(i).get(i), result);
            }

            results.add(result);
        }

        boolean condition = true;

        for (Integer result : results) {
            condition = condition && (result == 0);
        }

        return condition;
    }
}
