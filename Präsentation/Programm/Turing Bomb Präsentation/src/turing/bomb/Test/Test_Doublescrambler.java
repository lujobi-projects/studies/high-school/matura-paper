/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turing.bomb.Test;

import turing.bomb.RotorPart.Doublescrambler;
import turing.bomb.RotorPart.Reader;

/**
 *
 * @author Luzian
 */
public class Test_Doublescrambler {

    Reader reader = new Reader();
    int rotor1 = 5;
    int rotor2 = 1;
    int rotor3 = 2;
    int ukw = 1;
    Doublescrambler ds1 = new Doublescrambler(reader.getRotor(rotor1), reader.getRotor(rotor2), reader.getRotor(rotor3), reader.getReflector(ukw), reader.getInvRotor(rotor3), reader.getInvRotor(rotor2), reader.getInvRotor(rotor1));

    public Test_Doublescrambler() {

        ds1.setPosition(15000);
        System.out.println((char) (ds1.getPos1() + 65) + "," + (char) (ds1.getPos2() + 65) + "," + (char) (ds1.getPos3() + 65));

        for (int i = 0; i < 20; i++) {
            System.out.print(ds1.input(0) + "\t");
            ds1.input(i);
            ds1.turnOneStep();
        }

        System.out.println();

    }

}
