package turing.bomb.RotorPart;

import Interfaces.TuringBombConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Liest aus .txt-Dateien die Permutationen aus, die durch die einzelnen Rotoren
 * durchegührt werden und speichert sie in einer Liste.
 *
 * @author Luzian
 */
public class Reader implements TuringBombConstants {

    /**
     * Generiert den Reader.
     */
    public Reader() {
    }

    /**
     * Gibt eine Liste aus den Permutationen der Rotoren aus. Die Liste sieht
     * folgendermassen aus: an der ersten Stelle der Liste steht der Wert, mit
     * welchem der erste Wert vertauscht werden soll.
     *
     * @param rotor Nummer des Roters, dessen Liste der Permutationen
     * zurückgegeben werden soll.
     * @return Liste der Permutationen des ausgewählten Rotors.
     */
    public int[] getRotor(int rotor) {
        return loadDataFromFile(PATH_ROTOR + rotor + FILEEXT_TEXT, false);
    }

    /**
     * Gibt eine Liste aus den Inversen der Permutationen der Rotoren aus. Die
     * Liste sieht folgendermassen aus: an der ersten Stelle der Liste steht der
     * Wert, mit welchem der erste Wert vertauscht werden soll.
     *
     * @param rotor Nummer des Roters, dessen Liste der Permutationen
     * zurückgegeben werden soll.
     * @return Liste der Inversen der Permutationen des ausgewählten Rotors.
     */
    public int[] getInvRotor(int rotor) {
        return loadDataFromFile(PATH_ROTOR + rotor + FILEEXT_TEXT, true);
    }

    /**
     * Gibt eine Liste aus den Permutationen der Umkehrwalzen aus. Die Liste
     * sieht folgendermassen aus: an der ersten Stelle der Liste steht der Wert,
     * mit welchem der erste Wert vertauscht werden soll.
     *
     * @param rotor "Nummer" der Umkehrwalze, deren Liste der Permutationen
     * zurückgegeben werden soll.
     * @return Liste der Permutationen des ausgewählten Rotors.
     */
    public int[] getReflector(int rotor) {
        return loadDataFromFile(PATH_UKW + rotor + FILEEXT_TEXT, false);
    }

    private int[] loadDataFromFile(String filepath, boolean inv) {

        int[] list = new int[DEFAULTCHARNUMBER];
        int[] list_inv = new int[DEFAULTCHARNUMBER];

        try {
            InputStreamReader isr = new InputStreamReader(getClass().getResource(filepath).openStream(), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line;
            line = br.readLine().toUpperCase().replaceAll(" ", "");

            for (int i = 0; i < DEFAULTCHARNUMBER; i++) {
                list[i] = line.charAt(i) - CHARVALUE;
                list_inv[list[i]] = i;
            }
        } catch (Exception ex) {
        }

        if (inv) {
            return list_inv;
        } else {
            return list;
        }
    }
}
