package turing.bomb.RotorPart;

import Interfaces.TuringBombConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Erstellt eine Liste aus allen möglichen Rotorpositionen, also Anordnung der
 * einzelnen Rotoren und Umkehrwalze.
 *
 * @author Luzian
 */
public class AllWheelOrders implements TuringBombConstants {

    private final ArrayList<WheelOrder> possibility = new ArrayList<>();

    /**
     * Lädt die eine Liste (aus Rotorpositionen) aller Möglichkeiten, wie die
     * Rotoren und Umkehrwalzen angeordnet sein können.
     */
    public AllWheelOrders() {
        loadDataFromFile();
    }

    /**
     * Gibt die Rotorposition aus der Liste nach gesuchter Position zurück.
     *
     * @param a Position der Möglichkeit in der Liste
     * @return gesuchte Rotorposition
     */
    public WheelOrder getWheelOrder(int a) {
        return possibility.get(a);
    }

    /**
     * Gibt die Grösse der Liste, die durch Possibilities erstellt wurde,
     * zurück.
     *
     * @return Grösse der Liste, die durch Possibilities erstellt wurde
     */
    public int getSize() {
        return possibility.size();
    }

    private void loadDataFromFile() {
        try {
            String filepath = PATH_RESOURCES + "WheelOrders" + FILEEXT_TEXT;
            InputStreamReader isr = new InputStreamReader(getClass().getResource(filepath).openStream(), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line;

            while ((line = br.readLine()) != null) {
                String[] arr = line.split(",");
                if (arr.length == 4) {
                    WheelOrder rp = new WheelOrder(Integer.parseInt(arr[2].trim()), Integer.parseInt(arr[1].trim()), Integer.parseInt(arr[0].trim()), Integer.parseInt(arr[3].trim()));
                    possibility.add(rp);
                }
            }
        } catch (IOException | NumberFormatException ex) {
        }
    }

    /**
     * Sucht die den Rotoren entsprechende eindeutige Nummer für die
     * Rotorposition, bzw. die Stelle an der die Rotorposition in der lisste
     * aller Possibilites steht.
     *
     * @param rotor1 rechter Rotor
     * @param rotor2 mittlerer Rotor
     * @param rotor3 linker Rotor
     * @param ukw Umkehrwalze
     * @return eindeutige Nummer für die Rotorposition
     */
    public int getPossNr(int rotor1, int rotor2, int rotor3, int ukw) {
        for (int i = 0; i < possibility.size(); i++) {
            WheelOrder rp = possibility.get(i);
            if (rp.getRotor1() == rotor1 && rp.getRotor2() == rotor2 && rp.getRotor3() == rotor3 && rp.getUkw() == ukw) {
                return i;
            }
        }
        return 180;
    }
}
