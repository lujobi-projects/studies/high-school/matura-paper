package turing.bomb.GUI;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import turing.bomb.TuringBomb.Output;
import Interfaces.TuringBombConstants;

/**
 *
 * @author Luzian
 */
public class TableModel extends AbstractTableModel implements TuringBombConstants {

    private final ArrayList<Output> outputs;
    private final boolean showInNumbers;

    /**
     *
     * @param outputs
     * @param showInNumbers
     */
    public TableModel(ArrayList<Output> outputs, boolean showInNumbers) {
        this.outputs = outputs;
        this.showInNumbers = showInNumbers;
    }

    @Override
    public int getRowCount() {
        return outputs.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "Nr:";
        } else if (column == 1) {
            return "UKW:";
        } else if (column == 2) {
            return "Rot. L:";
        } else if (column == 3) {
            return "Rot. M:";
        } else if (column == 4) {
            return "Rot. R:";
        } else if (column == 5) {
            return "Pos. L:";
        } else if (column == 6) {
            return "Pos. M:";
        } else if (column == 7) {
            return "Pos. R:";
        } else if (column == 8) {
            return "Diagonalboard";
        }
        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Output output = outputs.get(rowIndex);

        if (columnIndex == 0) {
            return rowIndex + 1;
        } else if (columnIndex == 1 && showInNumbers) {
            return output.getUkw();
        } else if (columnIndex == 1 && !showInNumbers) {
            return output.getUkwAsString();
        } else if (columnIndex == 2 && showInNumbers) {
            return output.getRotor3();
        } else if (columnIndex == 2 && !showInNumbers) {
            return output.getRotor3AsString();
        } else if (columnIndex == 3 && showInNumbers) {
            return output.getRotor2();
        } else if (columnIndex == 3 && !showInNumbers) {
            return output.getRotor2AsString();
        } else if (columnIndex == 4 && showInNumbers) {
            return output.getRotor1();
        } else if (columnIndex == 4 && !showInNumbers) {
            return output.getRotor1AsString();
        } else if (columnIndex == 5 && showInNumbers) {
            return output.getPos3();
        } else if (columnIndex == 5 && !showInNumbers) {
            return output.getPos3AsString();
        } else if (columnIndex == 6 && showInNumbers) {
            return output.getPos2();
        } else if (columnIndex == 6 && !showInNumbers) {
            return output.getPos2AsString();
        } else if (columnIndex == 7 && showInNumbers) {
            return output.getPos1();
        } else if (columnIndex == 7 && !showInNumbers) {
            return output.getPos1AsString();
        } else if (columnIndex == 8 && showInNumbers) {
            return output.getDiagonalBoardOutput();
        } else if (columnIndex == 8 && !showInNumbers) {
            return output.getDiagonalBoardOutputAsString();
        }
        return null;
    }
}
