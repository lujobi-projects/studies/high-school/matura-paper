package turing.bomb.TuringBomb;

import java.util.ArrayList;
import turing.bomb.Diagonalboard.Diagonalboard;
import turing.bomb.RotorPart.RotorPart;
import turing.bomb.RotorPart.Solution;
import Interfaces.TuringBombConstants;

/**
 * Diese Klasse fasst die Ergebnisse von RotorPart und von Diagonalboard
 * zusammen und gibt diese in Forem einer Liste von Output-Objekten zurück.
 *
 * @author Luzian
 */
public class TuringBomb implements TuringBombConstants {

    private final ArrayList<ArrayList<Integer>> loops;
    private final ArrayList<String> connections;
    int cable;
    int wire;

    /**
     * Generiert eine Turingbombe mit folgendem Set-Up.
     *
     * @param loops Liste aus Listen von einzelnen Loops
     * @param connections Verbindungen für das Diagonalboard.
     * @param testregister Buchstabe, wo das Testregister angeschlossen wird
     * @param test Testbuchstabe, welcher getestet werden soll ob er mit dem
     * Buchstaben gesteckert ist, an welchen das Testregister angeschlossen ist.
     */
    public TuringBomb(ArrayList<ArrayList<Integer>> loops, ArrayList<String> connections, String testregister, String test) {
        this.loops = loops;
        this.connections = connections;
        cable = testregister.toUpperCase().charAt(0) - CHARVALUE;
        wire = test.toUpperCase().charAt(0) - CHARVALUE;
    }

    /**
     * Sucht alle Möglichkeiten ab, und jagt sie, sofern sie als Lösungen in
     * Frage kommen, durch das Diagonalboard und gibt sie als Liste von
     * Output-Objekten aus.
     *
     * @param differenceToBeginning Differenz zwischen Anfang des Cribs und
     * Anfang des verschlüsselten Textes
     * @param useDiagonalboard true wenn das Diagonalboard verwendet werden soll
     * @return Liste der zusammengefasssten Ergebnisse aus RotorPart und
     * Diagonalboard
     */
    public ArrayList<Output> findAllPossibilities(int differenceToBeginning, boolean useDiagonalboard) {
        RotorPart rp = new RotorPart(loops);
        ArrayList<Solution> rpOutput = rp.findAllPossibilities(differenceToBeginning, cable);
        return diagonalBoard(rpOutput, useDiagonalboard);
    }

    /**
     * Sucht eine Liste von Möglichkeiten ab, und jagt sie, sofern sie als
     * Lösungen in Frage kommen, durch das Diagonalboard und gibt sie als Liste
     * von Output-Objekten aus.
     *
     * @param possibilities Liste von RotorPosition-Objekten, welche abgesucht
     * werden sollen.
     * @param differenceToBeginning Differenz zwischen Anfang des Cribs und
     * Anfang des verschlüsselten Textes
     * @param useDiagonalboard true wenn das Diagonalboard verwendet werden soll
     * @return Liste der zusammengefasssten Ergebnisse aus RotorPart und
     * Diagonalboard
     */
    public ArrayList<Output> findPossibilities(ArrayList<Integer> possibilities, int differenceToBeginning, boolean useDiagonalboard) {
        RotorPart rp = new RotorPart(loops);
        ArrayList<Solution> rpOutput = rp.findPossibilities(possibilities, differenceToBeginning, cable);
        return diagonalBoard(rpOutput, useDiagonalboard);
    }

    private ArrayList<Output> diagonalBoard(ArrayList<Solution> solutions, boolean useDiagonalboard) {
        ArrayList<Output> output = new ArrayList<>();

        if (useDiagonalboard) {
            for (Solution solution : solutions) {
                Diagonalboard db = new Diagonalboard(connections, solution);
                output.add(new Output(solution.getRotorPosition(), solution.getStartingposition(), db.input(cable, wire)));
            }
        } else {
            ArrayList<Integer> db = new ArrayList();
            db.add(MULTIPLICATIONSIGN_VALUE);
            for (Solution solution : solutions) {
                output.add(new Output(solution.getRotorPosition(), solution.getStartingposition(), db));
            }
        }
        return output;
    }
}
