package turing.bomb.TuringBomb;

import java.util.ArrayList;
import turing.bomb.RotorPart.WheelOrder;

/**
 * Output ist Solution sehr ähnlich, wird jedoch durch den Output des
 * Diagonalboards ergänzt und kann die Lösungen als String ausgeben.
 *
 * @author Luzian
 */
public class Output implements Interfaces.TuringBombConstants {

    private final WheelOrder wo;
    private final int startingposition;
    private final ArrayList<Integer> diagonalBoardOutput;
    private final int pos_1;
    private final int pos_2;
    private final int pos_3;

    /**
     * Erstellt eine neue Lösung, bestehend aus der Lösung des Rotor Parts und
     * dem Output des Diagonalboards
     *
     * @param rp Rotorposition der Lösung
     * @param startingposition eindeutiger Wert der EInstellung des
     * Doublescramblers der Lösung
     * @param diagonalBoardOutput Ausgabe des Diagonalboards
     */
    public Output(WheelOrder rp, int startingposition, ArrayList<Integer> diagonalBoardOutput) {
        this.wo = rp;
        this.startingposition = startingposition;
        this.diagonalBoardOutput = diagonalBoardOutput;
        pos_1 = this.startingposition % DEFAULTCHARNUMBER;
        pos_2 = (int) this.startingposition % (DEFAULTCHARNUMBER * DEFAULTCHARNUMBER) / DEFAULTCHARNUMBER;
        pos_3 = (int) this.startingposition / (DEFAULTCHARNUMBER * DEFAULTCHARNUMBER);
    }

    /**
     * Gibt die Nummer der Umkehrwalze zurück
     *
     * @return Nummer der Umkehrwalze
     */
    public int getUkw() {
        return wo.getUkw();
    }

    /**
     * Gibt die "Nummer" der Umkehrwalze zurück 1=A, 2=B, 3=C
     *
     * @return "Nummer" der Umkehrwalze
     */
    public String getUkwAsString() {
        int ukw = wo.getUkw();
        if (ukw == 1) {
            return "A";
        } else if (ukw == 2) {
            return "B";
        } else if (ukw == 3) {
            return "C";
        }
        return null;
    }

    /**
     * Gibt die Nummer des rechten Rotors als römische Zahl zurück
     *
     * @return Nummer des rechten Rotors
     */
    public int getRotor1() {
        return wo.getRotor1();
    }

    /**
     * Gibt die Nummer des rechten Rotors als römische Zahl zurück
     *
     * @return römische Zahl der Nummer des rechten Rotors
     */
    public String getRotor1AsString() {
        return evaluateRotorString(wo.getRotor1());
    }

    /**
     * Gibt die Nummer des mittleren Rotors zurück
     *
     * @return Nummer des mittleren Rotors
     */
    public int getRotor2() {
        return wo.getRotor2();
    }

    /**
     * Gibt die Nummer des mittleren Rotors als römische Zahl zurück
     *
     * @return römische Zahl der Nummer des mittlern Rotors
     */
    public String getRotor2AsString() {
        return evaluateRotorString(wo.getRotor2());
    }

    /**
     * Gibt die Nummer des linken Rotors zurück
     *
     * @return Nummer des linken Rotors
     */
    public int getRotor3() {
        return wo.getRotor3();
    }

    /**
     * Gibt die Nummer des linken Rotors als römische Zahl zurück
     *
     * @return römische Zahl der Nummer des linekn Rotors
     */
    public String getRotor3AsString() {
        return evaluateRotorString(wo.getRotor3());
    }

    /**
     *
     * @return
     */
    public int getPos1() {
        return pos_1;
    }

    /**
     *
     * @return
     */
    public String getPos1AsString() {
        return "" + (char) (pos_1 + CHARVALUE);
    }

    /**
     *
     * @return
     */
    public int getPos2() {
        return pos_2;
    }

    /**
     *
     * @return
     */
    public String getPos2AsString() {
        return "" + (char) (pos_2 + CHARVALUE);
    }

    /**
     *
     * @return
     */
    public int getPos3() {
        return pos_3;
    }

    /**
     *
     * @return
     */
    public String getPos3AsString() {
        return "" + (char) (pos_3 + CHARVALUE);
    }

    /**
     *
     * @return
     */
    public String getDiagonalBoardOutput() {
        String output = ""; //Unterschied wenn leer
        if (diagonalBoardOutput.isEmpty()) {
            return "fail";
        } else if (diagonalBoardOutput.get(0) == -23 && diagonalBoardOutput.size() == 1) {
            return "*";
        }
        for (Integer dbOutput : diagonalBoardOutput) {
            if (dbOutput == MULTIPLICATIONSIGN_VALUE) {
                output = output + "*  ";
            } else {
                output = output + dbOutput + "  ";
            }

        }
        return output;
    }

    /**
     *
     * @return
     */
    public String getDiagonalBoardOutputAsString() {
        String output = ""; //Unterschied wenn leer
        if (diagonalBoardOutput.isEmpty()) {
            return "fail";
        }
        for (Integer dbOutput : diagonalBoardOutput) {
            output = output + (char) (dbOutput + CHARVALUE) + "  ";
        }
        return output;
    }

    /**
     *
     * @return
     */
    public int getStartingposition() {
        return startingposition;
    }

    /**
     *
     * @return
     */
    public WheelOrder getRotorposition() {
        return wo;
    }

    private String evaluateRotorString(int rotor) {
        if (rotor == 1) {
            return "I";
        } else if (rotor == 2) {
            return "II";
        } else if (rotor == 3) {
            return "III";
        } else if (rotor == 4) {
            return "IV";
        } else if (rotor == 5) {
            return "V";
        }
        return null;
    }
}
