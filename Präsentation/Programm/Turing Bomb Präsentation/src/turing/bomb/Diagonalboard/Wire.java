package turing.bomb.Diagonalboard;

import java.util.ArrayList;

/**
 * Stellt ein Draht des Diagonalboards dar
 * @author Luzian
 */
public class Wire {

    private final ArrayList<Integer> connectedTo;
    private boolean processed;

    /**
     * generiert ein Draht
     */
    public Wire() {
        this.connectedTo = new ArrayList();
        processed = false;
    }

    /**
     *fügt eine Verbindung zu einem anderen Draht hinzu
     * @param wire nummer des verbundenen Draht
     */
    public void addConnection(int wire) {
        connectedTo.add(wire);
    }

    /**
     *gibt alle Verbindungen zu einem anderen Draht zurück
     * @return Liste aller Verbindungen
     */
    public ArrayList<Integer> getConnections() {
        return connectedTo;
    }

    /**
     *Setzt den Draht auf abgearbeitet
     */
    public void setToProcessed() {
        processed = true;
    }

    /**
     *gibt zurück ob der Draht schon bearbeitet wurde
     * @return Wahrheitswert ob der Draht wurde bearbeitet
     */
    public boolean isAlreadyProcessed() {
        return processed;
    }
}
