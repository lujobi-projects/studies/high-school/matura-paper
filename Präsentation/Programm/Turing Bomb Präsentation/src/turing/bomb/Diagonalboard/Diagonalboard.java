package turing.bomb.Diagonalboard;

import java.util.ArrayList;
import turing.bomb.RotorPart.Settings;
import turing.bomb.RotorPart.Solution;
import Interfaces.TuringBombConstants;

/**
 *Gordon Welchmanns Diagonalboar der Turing-Bombe
 * @author Luzian
 */
public class Diagonalboard implements TuringBombConstants {

    private final boolean[] matrix = new boolean[DEFAULTCHARNUMBER * DEFAULTCHARNUMBER];
    private final Wire[] wires = new Wire[DEFAULTCHARNUMBER * DEFAULTCHARNUMBER];
    private final Settings settings;
    private final Solution adjustedSolution;

    /**
     *Generiert ein Diagonalboard für ein Solutions Objekt
     * @param connections Verbindungen des Diagonalboards als Liste von Strings
     * @param solution Eine Lösung des RotorParts als Solutions-Objekt
     */
    public Diagonalboard(ArrayList<String> connections, Solution solution) {
        adjustedSolution = new Solution(solution.getRotorPosition(), solution.getStartingposition()+1);
        settings = new Settings(adjustedSolution.getRotorPosition(), adjustedSolution.getStartingposition());
        for (int i = 0; i < matrix.length; i++) { //"Lampen" auf aus gesetzt. 
            matrix[i] = false;
        }

        for (int i = 0; i < wires.length; i++) { //Diagonalboard und dessen Verbindungen wird erstellt
            wires[i] = new Wire();
            int connection = getCable(i) + getWire(i) * DEFAULTCHARNUMBER;
            if (connection != i) {
                wires[i].addConnection(connection);
            }
        }

        for (String connection : connections) { //Die Verbindungen durch die Doublescrambler werden ertellt.
            for (int a = 0; a < 2; a++) {
                int startCableId = getId(connection.toUpperCase().charAt(a) - CHARVALUE, 0);
                int endCableId = getId(connection.toUpperCase().charAt(1 - a) - CHARVALUE, 0);
                for (int b = 0; b < DEFAULTCHARNUMBER; b++) {
                    wires[startCableId + b].addConnection(endCableId + settings.input(0, b));
                }
            }
            settings.turnDS();
        }
    }

    /**
     *"Startet" das Diagonalboard.
     * @param cable Wert des Kabels an den das Testregisters angeschlossen ist
     * @param wire Wert des Drahts, der am Testregister ausgewählt wurde
     * @return ein ganzes Kabel (das, an das das Tstregistes angeschlossen wurde) mit dem Wert von (*) wenn Spannung auftritt. 
     */
    public ArrayList<Integer> input(int cable, int wire) {
        int inputId = getId(cable, wire);
        matrix[inputId] = true;

        boolean changesPerformed = true;
        while (changesPerformed) {
            changesPerformed = false;
            for (int i = 0; i < matrix.length; i++) {//testen!
                Wire actualWire = wires[i];
                if (matrix[i] && !actualWire.isAlreadyProcessed()) {
                    for (Integer connection : actualWire.getConnections()) {
                        matrix[connection] = true;
                    }
                    actualWire.setToProcessed();
                    changesPerformed = true;
                }
            }
        }

        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            if (!matrix[getId(getCable(inputId), i)]) {
                result.add(i);
            } else {
                result.add(MULTIPLICATIONSIGN_VALUE);
            }
        }
        return result;
     }

    private int getId(int cable, int wire) {
        return DEFAULTCHARNUMBER * cable + wire;
    }

    private int getCable(int id) {
        return id / DEFAULTCHARNUMBER;
    }

    private int getWire(int id) {
        return id % DEFAULTCHARNUMBER;
    }
}
