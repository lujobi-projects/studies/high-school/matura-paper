\select@language {ngerman}
\contentsline {chapter}{\numberline {1}Vorwort}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Einleitung}{4}{chapter.2}
\contentsline {chapter}{\numberline {3}Enigma}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Geschichte}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Funktion}{6}{section.3.2}
\contentsline {section}{\numberline {3.3}Schw\IeC {\"a}chen}{7}{section.3.3}
\contentsline {chapter}{\numberline {4}Turing-Bombe}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Geschichte und Entwicklung}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Crib}{9}{section.4.2}
\contentsline {section}{\numberline {4.3}Bestimmung der Rotorposition}{10}{section.4.3}
\contentsline {section}{\numberline {4.4}Diagonalboard}{12}{section.4.4}
\contentsline {section}{\numberline {4.5}Was passiert, wenn die Turing-Bombe anh\IeC {\"a}lt}{13}{section.4.5}
\contentsline {section}{\numberline {4.6}Wo funktioniert sie \IeC {\"u}berhaupt?}{14}{section.4.6}
\contentsline {chapter}{\numberline {5}Programm}{15}{chapter.5}
\contentsline {section}{\numberline {5.1}Struktur}{15}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}TuringBomb}{15}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Doublescrambler}{15}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}RotorPart}{16}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Diagonalboard}{17}{subsection.5.1.4}
\contentsline {section}{\numberline {5.2}Screenshot}{18}{section.5.2}
\contentsline {section}{\numberline {5.3}Vergleich zur echten Turing-Bombe}{19}{section.5.3}
\contentsline {section}{\numberline {5.4}Auswertung}{19}{section.5.4}
\contentsline {section}{\numberline {5.5}Ausblick}{19}{section.5.5}
\contentsline {chapter}{\numberline {6}Fazit}{21}{chapter.6}
\contentsline {chapter}{\numberline {7}Bibiliografie}{22}{chapter.7}
\contentsline {chapter}{\numberline {8}Hilfsmittel}{23}{chapter.8}
