[33mcommit 7da70c13839fede33feb981f464b9871fba66494[m[33m ([1;36mHEAD[m[33m, [1;32mmaster[m[33m)[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Mon May 4 19:39:02 2015 +0200

    Klasse Constants eingefügt.
    Ordner Resources erstellt und Textdateien für jeden Rotor einzeln erstllt

[33mcommit 200ccbd942dffbe3ebf8bf50a264a3c81df5661b[m[33m ([1;31morigin/master[m[33m)[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Sun May 3 19:15:37 2015 +0200

    Beginn von Klasse ReadOut

[33mcommit 5abdb7fe6e32b37dd7d04f0a4e0cfc721810a041[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Sun May 3 18:20:05 2015 +0200

    Verbesserung der Funktion input der Klasse Doublescrambler durch Verwendung des Modulo-Operators, sodass die Listen nur halb so lang sein müssen

[33mcommit 0d1ae3a343c352bc380a37521ef9bfe66c3572ec[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Sun May 3 17:47:51 2015 +0200

    Klasse Doublescrambler angepasst, sodass sie nun mit der Enigma übereinstimmt

[33mcommit a9e2b4254f265fdf8ea7928e368888aed65878f2[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Sun Apr 5 22:33:09 2015 +0200

    *Neu kann einr RotorPosition-Objekt als Rotor- und Ukwanordnung übergeben übergeben werden
    *setPosition wurde überarbeitet da es fehler hatte.

[33mcommit 63a41d3cf1f93136e38c2e9102041d6fd00a0cc1[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Fri Apr 3 23:23:34 2015 +0200

    *settingsDS ist nun eine ArrayList
    *neue Klasse aus der alle möglichen Walzenpositionen geladen werden können

[33mcommit 2a63aea70606764caae2f602d182c13741720765[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Wed Mar 25 18:45:51 2015 +0100

    *Klasse JTuringBomb hinzugefügt
    *Klasse TuringBomb hinzugefügt
    *Klasse Doublescrambler überarbeitet:
    	-Methode SetPosition hinzugefügt => Setzt die Startposition eines Doublescramblers
    *Klasse Settings überarbeitet:
    	-Konstruktor überarbeitet:
    		>nun wird eine Liste von Doublescramblern generiert
    	-evaluate... -Methoden hinzugefügt so dass im Konstruktor nur noch 4 Werte für die Rotoren übergeben werden
    	-turnDS-Methode hinzugefügt, welche alle Doublescrambler der Liste settingsDS um eine Position dreht

[33mcommit b0fa752ab7c28c350837d12f1715d7b80160da55[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Sun Mar 22 18:13:29 2015 +0100

    *Wiring.txt besteht nun aus Buchstaben und nicht aus Zahlen
    *Rotoren sind drehbar, also die Position erhielt eine Variable, Richtigkeit aber noch nicht getestet

[33mcommit 1110d090672225c9f31b5d6b240b5d0b5e4521dd[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Thu Mar 19 10:29:22 2015 +0100

    LoadDataFromFile repariert.

[33mcommit 9d900f94bc9dcc6ba56636b6babb592d678179f5[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Tue Mar 17 21:06:04 2015 +0100

    Versuch loadDataFromFile zu überarbeiten

[33mcommit c0255559fd8f85800bb7eee121604eb9e7d9dec3[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Tue Mar 17 20:14:47 2015 +0100

    *Klasse Doublescrambler erstellt
    	-Konstructor
    	-Methode input
    *Klasse Settings erstellt
    	-Methode loadDataFromFile hinzugefügt (funktionirt noch nicht)
    * Klasse mainClass erstellt (vorübergehend MainClass)
    *Dateien Wiring.txt und Wiring_ visual.txt erstellt:
    	enthalten die Verkabelunges der einzelnen Rotoren

[33mcommit 5856ddebfcb8d5db6ac56f51c2dafd18cbee321e[m
Author: Thomas Jampen <thomas.jampen@gymkirchenfeld.ch>
Date:   Fri Mar 13 16:16:24 2015 +0100

    Bewertungsschema hinzugefügt.

[33mcommit a186c31e1f4ac77475e01f61e2f0300f12c8d5ec[m
Author: Thomas Jampen <thomas.jampen@gymkirchenfeld.ch>
Date:   Fri Mar 13 16:10:43 2015 +0100

    Journal erstellt.

[33mcommit 347781d057580d818ce920f444614adbdfa3762d[m
Author: Luzian Bieri <luzian.bieri@kinet.ch>
Date:   Fri Mar 13 16:07:20 2015 +0100

    .gitignore hinzugefügt.
