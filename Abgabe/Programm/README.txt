+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
�---------------------------------------------------------------------------------�
�---------------------------------------------------------------------------------�
�------------------------MANUAL for the TURINGBOMB-PROGRAM------------------------�
�---------------------------------------------------------------------------------�
�-----------------------------created by Luzian Bieri-----------------------------�
�-----------------------on the occasion of a matura thesis------------------------�
�------------------------------Tutor: Thomas Jampen-------------------------------�
�---------------------------------------------------------------------------------�
�----------------------Churchfield Grammar School Bern, 2015----------------------�
�---------------------------------------------------------------------------------�
�---------------------------contact: l.j.bieri@gmail.com--------------------------�
�---------------------------------------------------------------------------------�
�---------------------------------------------------------------------------------�
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+

USERS'S GUIDE
�������������

  1. INSTALLATION

     The program is a java-application and doesn't have to be installed. Just
     copy the jar-archive to your desired folder. You should be able to execute
     the application with a double-click on the .jar-file. If not, you might not 
     have installed JRE (Java Runtime Environment) on your computer. In that case,
     go to http://www.oracle.com and download the latest version, then try again.
     (Severin M�nger, 2010)

  2. THE PROGRAM

     If the program is run, a window opens, where one can choose between three
     different types of the program. One version simulates a original version of an
     original, another exhausts the whole key space and the third enabels to choose 
     more than three wheelorders. To make that choice there are JRadioButtons and 
     to comfirm a jButton. Additionally there is a JButton to turn exit the program.

     If one choice has been made and confirmed another window opens. In there are
     several JTextFields where loops (which must be found by hand), diagonalboard-
     connections, the difference to the beginning of the text and the wheelorders
     can be put in. Additionally there are two JSpinners for the testregister and 
     the chosen wire, three JButtons to load the last input, to start the Bombe and 
     to return to the starting page, two JRadioButtons to chose between the display 
     as chacters or numbers and one JTable, where the solution is displayed.

     In order to start decyphering, first find a crib and a possible position in the 
     cypher text. Then derive from this the loops. (All this must be done by hand)
     Start the program and chose one type of the bombe in the starting window. Then 
     fill in all the required inputs and press the start button. The result is given
     out in the table below all the inputs.

  3. KNOWN ISSUES
     
     Because in a compiled jar-file a .txt-file can not be edited the load-last-
     input-button doesn't work as expected. So it creates several .txt-files in the
     folder, where the .jar-file is placed.

     Difference to the beginning and the diagonalboard don't work together.

