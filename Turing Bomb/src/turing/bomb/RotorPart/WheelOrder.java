package turing.bomb.RotorPart;

/**
 * Dieses Objekt stellt eine die Rotorposition, also die Anordnung der
 * Umkehrwalze und Rotoren dar.
 *
 * @author Luzian
 */
public class WheelOrder {

    private final int rotor1;
    private final int rotor2;
    private final int rotor3;
    private final int ukw;

    /**
     * Erstellt eine Rotorposition, also eine Anordnung der Rotoren und
     * Umkehrwalzen.
     *
     * @param rotor1 rechter Rotor
     * @param rotor2 mittlerer Rotor
     * @param rotor3 linker Rotor
     * @param ukw Umkehrwalze
     */
    public WheelOrder(int rotor1, int rotor2, int rotor3, int ukw) {
        this.rotor1 = rotor1;
        this.rotor2 = rotor2;
        this.rotor3 = rotor3;
        this.ukw = ukw;
    }

    /**
     * Gibt die Nummer des rechten Rotors zurück
     *
     * @return Nummer des rechten Rotors
     */
    public int getRotor1() {
        return rotor1;
    }

    /**
     * Gibt die Nummer des mittleren Rotors zurück
     *
     * @return Nummer des mittleren Rotors
     */
    public int getRotor2() {
        return rotor2;
    }

    /**
     * Gibt die Nummer des linken Rotors zurück
     *
     * @return Nummer des linken Rotors
     */
    public int getRotor3() {
        return rotor3;
    }

    /**
     * Gibt die Nummer der Umkehrwalze zurück
     *
     * @return Nummer der Umkehrwalze
     */
    public int getUkw() {
        return ukw;
    }

    /**
     * Gibt die "Nummer" der Umkehrwalze zurück 1=A, 2=B, 3=C
     *
     * @return "Nummer" der Umkehrwalze
     */
    public String ukwAsString() {
        if (ukw == 1) {
            return "A";
        } else if (ukw == 2) {
            return "B";
        } else if (ukw == 3) {
            return "C";
        }
        return null;
    }

    @Override
    public String toString() {
        return "" + ukwAsString() + rotor3 + rotor2 + rotor1;
    }
}
