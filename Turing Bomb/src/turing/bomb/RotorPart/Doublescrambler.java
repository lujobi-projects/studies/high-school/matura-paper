package turing.bomb.RotorPart;

import Interfaces.TuringBombConstants;

/**
 * Doublsecrabler besteht aus mehren Rotoren, welche weitergedreht werden
 * können. In den Dublescrambler kann ein Buchstaeb hinengegeben werden, sodass
 * dieser verschlüsselt wird.
 *
 * @author Luzian
 */
public class Doublescrambler implements TuringBombConstants {

    private int[] rotor_1 = new int[DEFAULTCHARNUMBER];
    private int[] rotor_2 = new int[DEFAULTCHARNUMBER];
    private int[] rotor_3 = new int[DEFAULTCHARNUMBER];
    private int[] ukw = new int[DEFAULTCHARNUMBER];
    private int[] rotor_3_inv = new int[DEFAULTCHARNUMBER];
    private int[] rotor_2_inv = new int[DEFAULTCHARNUMBER];
    private int[] rotor_1_inv = new int[DEFAULTCHARNUMBER];
    private int pos_1 = 0;
    private int pos_2 = 0;
    private int pos_3 = 0;

    /**
     * Generiert den Doublescambler
     *
     * @param rotor_1 Liste der Permutationen, die der erste bzw. rechte Rotor
     * durchführt.
     * @param rotor_2 Liste der Permutationen, die der zweite bzw. mittlere
     * Rotor durchführt.
     * @param rotor_3 Liste der Permutationen, die der dritte bzw. linke Rotor
     * durchführt.
     * @param ukw Liste der Permutationen, die die Umkehrwalze durchführt.
     * @param rotor_3_inv Inverses der Liste der Permutationen, die der dritte
     * bzw. linke Rotor durchführt.
     * @param rotor_2_inv Inverses der Liste der Permutationen, die der zweite
     * bzw. mittlere Rotor durchführt.
     * @param rotor_1_inv Inverses der Liste der Permutationen, die der dritte
     * bzw. linke Rotor durchführt.
     */
    public Doublescrambler(int[] rotor_1, int[] rotor_2, int[] rotor_3, int[] ukw, int[] rotor_3_inv, int[] rotor_2_inv, int[] rotor_1_inv) {
        this.rotor_1 = rotor_1;
        this.rotor_2 = rotor_2;
        this.rotor_3 = rotor_3;
        this.ukw = ukw;
        this.rotor_3_inv = rotor_3_inv;
        this.rotor_2_inv = rotor_2_inv;
        this.rotor_1_inv = rotor_1_inv;
    }

    /**
     * Schickt den Wert eines Buchstabens durch den Doublescrambler.
     *
     * @param input Der Wert des Buchstabens, der durch den Doublescrambler
     * geschickt wird. A=0, B=1, usw.
     * @return Der Wert des ausgegebenen Buchstabens A=0, B=1, usw.
     */
    public int input(int input) {
        return (rotor_1_inv[(rotor_2_inv[(rotor_3_inv[(ukw[(rotor_3[(rotor_2[(rotor_1[(input + pos_1)
                % DEFAULTCHARNUMBER] + DEFAULTCHARNUMBER - pos_1 + pos_2) % DEFAULTCHARNUMBER] + DEFAULTCHARNUMBER - pos_2 + pos_3)
                % DEFAULTCHARNUMBER] + DEFAULTCHARNUMBER - pos_3) % DEFAULTCHARNUMBER] + pos_3) % DEFAULTCHARNUMBER] + DEFAULTCHARNUMBER - pos_3 + pos_2)
                % DEFAULTCHARNUMBER] + DEFAULTCHARNUMBER - pos_2 + pos_1) % DEFAULTCHARNUMBER] + DEFAULTCHARNUMBER - pos_1) % DEFAULTCHARNUMBER;
    }

    /**
     * Gibt die Rotorposition des ersten Rotos zurück
     *
     * @return Rotorposition des ersten Rotos
     */
    public int getPos1() {
        return pos_1;
    }

    /**
     * Gibt die Rotorposition des zweiten Rotos zurück
     *
     * @return Rotorposition des zweiten Rotos
     */
    public int getPos2() {
        return pos_2;
    }

    /**
     * Gibt die Rotorposition des dritten Rotos zurück
     *
     * @return Rotorposition des dritten Rotos
     */
    public int getPos3() {
        return pos_3;
    }

    /**
     * Dreht den Doublescrambler um eine Position weiter.
     */
    public void turnOneStep() {
        pos_1 = pos_1 + 1;
        if (pos_1 == DEFAULTCHARNUMBER) {
            pos_1 = 0;
            pos_2 = pos_2 + 1;
        }
        if (pos_2 == DEFAULTCHARNUMBER) {
            pos_2 = 0;
            pos_3 = pos_3 + 1;
        }
        if (pos_3 == DEFAULTCHARNUMBER) {
            pos_3 = 0;
        }
    }

    /**
     * Setzt die Position der Rotoren entsprechend dem eingegebenen Wert.
     *
     * @param position eindeutiger Wert für die Position der Rotoren: Rotor
     * rechts + 26*Rotor mitte + 26*26*Rotor links.
     */
    public void setPosition(int position) {
        pos_1 = position % DEFAULTCHARNUMBER;
        pos_2 = (int) position % (DEFAULTCHARNUMBER * DEFAULTCHARNUMBER) / DEFAULTCHARNUMBER;
        pos_3 = (int) position / (DEFAULTCHARNUMBER * DEFAULTCHARNUMBER);
    }
}
