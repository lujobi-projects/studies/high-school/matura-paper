package turing.bomb.RotorPart;

import Interfaces.TuringBombConstants;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * RotorPart ist der Teil, der Turingbombe, der die
 *
 * @author Luzian
 */
public class RotorPart implements TuringBombConstants {

    private final AllWheelOrders poss = new AllWheelOrders();
    private Settings settings;
    private ArrayList<ArrayList<Integer>> loopsTB = new ArrayList<>();

    /**
     * Erstellt eine Turingbombe auf nach den Loops die mitgegeben werden
     *
     * @param loops Liste von Loops
     */
    public RotorPart(ArrayList<Integer>... loops) {
        this.loopsTB.addAll(Arrays.asList(loops));
    }

    /**
     * Erstellt eine Turingbombe auf nach den Loops die mitgegeben werden
     *
     * @param loops Liste von Loops, welche wiederum als Listen dargestellt
     * werden
     */
    public RotorPart(ArrayList<ArrayList<Integer>> loops) {
        this.loopsTB = loops;
    }

    /**
     * Sucht alle Möglichkeiten ab, und gibt, sofern sie als Lösungen in Frage
     * kommen, sie als Liste von Solution-Objekten aus.
     *
     * @param differenceToBeginning Differenz zwischen Anfang des Cribs und
     * Anfang des verschlüsselten Textes
     * @param testregister Wert des Buchstabens an welchen das Testregister
     * angeschlossen werden soll. A=0, B=1, usw.
     * @return Liste von Solution-Objekten, welche als Lösungen in Frage kommen.
     */
    public ArrayList<Solution> findAllPossibilities(int differenceToBeginning, int testregister) {
        return findPossibilitiesFromTo(0, poss.getSize(), differenceToBeginning, testregister);
    }

    /**
     * Sucht eine Liste von Möglichkeiten ab, und gibt, sofern sie als Lösungen
     * in Frage kommen, sie als Liste von Solution-Objekten aus.
     *
     * @param possibilities Liste von RotorPosition-Objekten, welche abgesucht
     * werden sollen.
     * @param differenceToBeginning Differenz zwischen Anfang des Cribs und
     * Anfang des verschlüsselten Textes
     * @param testregister Wert des Buchstabens an welchen das Testregister
     * angeschlossen werden soll. A=0, B=1, usw.
     * @return Liste von Solution-Objekten, welche als Lösungen in Frage kommen.
     */
    public ArrayList<Solution> findPossibilities(ArrayList<Integer> possibilities, int differenceToBeginning, int testregister) {
        ArrayList<Solution> solutions = new ArrayList();
        for (Integer possibility : possibilities) {
            settings = new Settings(poss.getWheelOrder(possibility));
            for (int i = 0; i < MAXWHEELORDERNR; i++) {
                if (isSolution(settings, testregister)) {
                    solutions.add(new Solution(poss.getWheelOrder(possibility), (i + MAXWHEELORDERNR + MAXWHEELORDERNR - differenceToBeginning) % MAXWHEELORDERNR));
                }
                settings.turnDS();
            }
        }
        return solutions;
    }

    private ArrayList<Solution> findPossibilitiesFromTo(int start, int end, int differenceToBeginning, int testregister) {
        ArrayList<Solution> solutions = new ArrayList();
        for (int a = start; a < end; a++) {
            settings = new Settings(poss.getWheelOrder(a));
            for (int i = 0; i < MAXWHEELORDERNR; i++) {
                if (isSolution(settings, testregister)) {
                    solutions.add(new Solution(poss.getWheelOrder(a), (i + MAXWHEELORDERNR + MAXWHEELORDERNR - differenceToBeginning) % MAXWHEELORDERNR));
                }
                settings.turnDS();
            }
        }
        return solutions;
    }

    private boolean isSolution(Settings s, int testregister) {
        boolean isSolution = true;
        for (ArrayList<Integer> loopsTB1 : loopsTB) {

            int result = s.input(loopsTB1.get(0), 0);

            for (int c = 1; c < loopsTB1.size(); c++) {
                result = s.input(loopsTB1.get(c), result);
            }

            if (result != 0) {
                isSolution = false;
                break;
            }
        }
        return isSolution;
    }
}
