package turing.bomb.RotorPart;

import Interfaces.TuringBombConstants;

/**
 * Stellt die Lösung der RotorParts dar.
 *
 * @author Luzian
 */
public class Solution implements TuringBombConstants {

    private final WheelOrder wo;
    private final int startingposition;
    private final int pos_1;
    private final int pos_2;
    private final int pos_3;

    /**
     * Erstellt eine neue Lösung
     *
     * @param rp Rotorposition der Lösung
     * @param startingposition eindeutiger Wert der EInstellung des
     * Doublescramblers der Lösung
     */
    public Solution(WheelOrder rp, int startingposition) {
        this.wo = rp;
        this.startingposition = startingposition;
        pos_1 = this.startingposition % DEFAULTCHARNUMBER;
        pos_2 = (int) this.startingposition % (DEFAULTCHARNUMBER * DEFAULTCHARNUMBER) / DEFAULTCHARNUMBER;
        pos_3 = (int) this.startingposition / (DEFAULTCHARNUMBER * DEFAULTCHARNUMBER);

    }

    @Override
    public String toString() {
        return "" + wo.getRotor1() + "\t" + wo.getRotor2() + "\t" + wo.getRotor3() + "\t" + getUkwAsString() + "\t" + "\t" + pos_1 + "\t" + pos_2 + "\t" + pos_3 + "\t" + startingposition;
    }

    /**
     * Gibt die Rotorposition der Lösung zurück
     *
     * @return Rotorposition der Lösung
     */
    public WheelOrder getRotorPosition() {
        return wo;
    }

    /**
     * Gibt den eindeutigen Wert der EInstellung des Doublescramblers der Lösung
     * zurück
     *
     * @return eindeutiger Wert der EInstellung des Doublescramblers der Lösung
     */
    public int getStartingposition() {
        return startingposition;
    }

    private String getUkwAsString() {
        int ukw = wo.getUkw();
        if (ukw == 1) {
            return "A";
        } else if (ukw == 2) {
            return "B";
        } else if (ukw == 3) {
            return "C";
        }
        return null;
    }
}
