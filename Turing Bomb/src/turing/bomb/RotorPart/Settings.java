package turing.bomb.RotorPart;

import Interfaces.TuringBombConstants;
import java.util.ArrayList;

/**
 * Erstellt eine Liste von Doublecramblern, welche entsprechend einer Liste
 * eingestellt wird.
 *
 * @author Luzian
 */
public class Settings implements TuringBombConstants {

    private final ArrayList<Doublescrambler> settingsDS = new ArrayList<>();
    private final Reader reader = new Reader();
    private final int[] rotor1;
    private final int[] rotor2;
    private final int[] rotor3;
    private final int[] ukw;
    private final int[] invRotor3;
    private final int[] invRotor2;
    private final int[] invRotor1;

    /**
     * Setzt die Einstellungen der einzelnen Doublescrambler entsprechend der
     * mitgegebenen Liste.
     *
     * @param wo Rotorposition, also Anordnung der Rotoren und Umkehrwalzen, der
     * Doublescrambler
     * @param startingPositions Liste der Startpositionen der Doblescrambler
     */
    public Settings(WheelOrder wo, int... startingPositions) {
        rotor1 = reader.getRotor(wo.getRotor1());
        rotor2 = reader.getRotor(wo.getRotor2());
        rotor3 = reader.getRotor(wo.getRotor3());
        ukw = reader.getReflector(wo.getUkw());
        invRotor3 = reader.getInvRotor(wo.getRotor3());
        invRotor2 = reader.getInvRotor(wo.getRotor2());
        invRotor1 = reader.getInvRotor(wo.getRotor1());

        for (int a = 0; a < startingPositions.length; a++) {
            settingsDS.add(new Doublescrambler(rotor1, rotor2, rotor3, ukw, invRotor3, invRotor2, invRotor1));
            settingsDS.get(a).setPosition(startingPositions[a]);
        }
    }

    /**
     * etzt die Einstellungen der einzelnen Doublescrambler beginnend bei 0 bis
     * 25
     *
     * @param wo Rotorposition, also Anordnung der Rotoren und Umkehrwalzen, der
     * Doublescrambler
     */
    public Settings(WheelOrder wo) {
        rotor1 = reader.getRotor(wo.getRotor1());
        rotor2 = reader.getRotor(wo.getRotor2());
        rotor3 = reader.getRotor(wo.getRotor3());
        ukw = reader.getReflector(wo.getUkw());
        invRotor3 = reader.getInvRotor(wo.getRotor3());
        invRotor2 = reader.getInvRotor(wo.getRotor2());
        invRotor1 = reader.getInvRotor(wo.getRotor1());

        for (int a = 0; a < DEFAULTCHARNUMBER; a++) {
            settingsDS.add(new Doublescrambler(rotor1, rotor2, rotor3, ukw, invRotor3, invRotor2, invRotor1));
            settingsDS.get(a).setPosition(a);
        }
    }

    /**
     * Dreht alle Doublescrambler um eine Position weiter.
     */
    public void turnDS() {
        for (Doublescrambler ds : settingsDS) {
            ds.turnOneStep();
        }
    }

    /**
     * In einen bestimmten Doublescrambler wird der Wert eines Buchstabens
     * eingegeben.
     *
     * @param doublescrambler Nummer des Doublescramblers in der Liste
     * @param input Wert des Buchstabens, der durch den Doublescrambler
     * geschickt wird. A=0, B=1, usw.
     * @return Wert des ausgegebenen Buchstabens A=0, B=1, usw.
     */
    public int input(int doublescrambler, int input) {
        return settingsDS.get(doublescrambler).input(input);
    }
}
