package turing.bomb.GUI;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.TableColumn;
import turing.bomb.TuringBomb.Output;
import turing.bomb.TuringBomb.TuringBomb;
import Interfaces.TuringBombConstants;
import javax.swing.JSpinner;

/**
 *
 * @author Luzian
 */
public class WholeExhaustion extends javax.swing.JFrame implements TuringBombConstants {

    ArrayList<Output> outputs = new ArrayList<>();
    JTuringBomb jtb;

    private final String filename = "LastInputWholeExhaustion";

    /**
     *
     * @param jtb
     */
    public WholeExhaustion(JTuringBomb jtb) {
        initComponents();
        this.jtb = jtb;
        newTablemodel();
        charactersRadioButton.setSelected(true);
        JSpinner.DefaultEditor cableEditor = (JSpinner.DefaultEditor) cableSpinner.getEditor();
        cableEditor.getTextField().enable(false);
        JSpinner.DefaultEditor wireEditor = (JSpinner.DefaultEditor) wireSpinner.getEditor();
        wireEditor.getTextField().enable(false);
        resetColors();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        contentPanel = new javax.swing.JPanel();
        solutionTableScrollPane = new javax.swing.JScrollPane();
        solutionTable = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        charactersRadioButton = new javax.swing.JRadioButton();
        nrOfSolutionsField = new javax.swing.JTextField();
        numbersRadioButton = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cableSpinner = new javax.swing.JSpinner();
        loopField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();
        wireSpinner = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        startButton = new javax.swing.JButton();
        connectionsField = new javax.swing.JTextField();
        instructionScrollPane = new javax.swing.JScrollPane();
        instructionTextArea = new javax.swing.JTextArea();
        loadLastInputButton = new javax.swing.JButton();
        differenceToBeginningField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Turing Bomb - Whole Exhaustion - by Luzian Bieri");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(java.awt.Color.lightGray);
        setLocation(new java.awt.Point(0, 0));
        setResizable(false);

        contentPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        contentPanel.setDoubleBuffered(false);
        contentPanel.setPreferredSize(new java.awt.Dimension(800, 700));

        solutionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Solution Nr:", "Rotor 1:", "Rotor 2:", "Rotor 3:", "UKW:", "Position 1:", "Position 2:", "Position 3:", "Diagonalboard:"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        solutionTableScrollPane.setViewportView(solutionTable);
        if (solutionTable.getColumnModel().getColumnCount() > 0) {
            solutionTable.getColumnModel().getColumn(0).setResizable(false);
            solutionTable.getColumnModel().getColumn(1).setResizable(false);
            solutionTable.getColumnModel().getColumn(2).setResizable(false);
            solutionTable.getColumnModel().getColumn(3).setResizable(false);
            solutionTable.getColumnModel().getColumn(4).setResizable(false);
            solutionTable.getColumnModel().getColumn(5).setResizable(false);
            solutionTable.getColumnModel().getColumn(6).setResizable(false);
            solutionTable.getColumnModel().getColumn(7).setResizable(false);
            solutionTable.getColumnModel().getColumn(8).setResizable(false);
        }
        solutionTable.getAccessibleContext().setAccessibleName("");

        jLabel11.setText("Show in:");

        charactersRadioButton.setText("Characters");
        charactersRadioButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        charactersRadioButton.setDoubleBuffered(true);
        charactersRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                charactersRadioButtonActionPerformed(evt);
            }
        });

        nrOfSolutionsField.setEditable(false);
        nrOfSolutionsField.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        nrOfSolutionsField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nrOfSolutionsFieldActionPerformed(evt);
            }
        });

        numbersRadioButton.setText("Numbers");
        numbersRadioButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        numbersRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numbersRadioButtonActionPerformed(evt);
            }
        });

        jLabel5.setText("Number of solutions:");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Turing Bomb");

        jLabel6.setText("Loops:");

        jLabel2.setText("by Luzian Bieri");

        jLabel8.setText("Testregister:");

        cableSpinner.setModel(new javax.swing.SpinnerListModel(new String[] {" A", " B", " C", " D", " E", " F", " G", " H", " I", " J", " K", " L", " M", " N", " O", " P", " Q", " R", " S", " T", " U", " V", " W", " X", " Y", " Z"}));
        cableSpinner.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cableSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cableSpinnerStateChanged(evt);
            }
        });

        loopField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loopFieldActionPerformed(evt);
            }
        });

        jLabel9.setText("Wire:");

        backButton.setText("Back to Startingpage");
        backButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        wireSpinner.setModel(new javax.swing.SpinnerListModel(new String[] {" A", " B", " C", " D", " E", " F", " G", " H", " I", " J", " K", " L", " M", " N", " O", " P", " Q", " R", " S", " T", " U", " V", " W", " X", " Y", " Z"}));
        wireSpinner.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wireSpinner.setFocusCycleRoot(true);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Whole Exhaustion");

        jLabel10.setText("Diagonalboard-Connections:");

        startButton.setText("Start");
        startButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        connectionsField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectionsFieldActionPerformed(evt);
            }
        });

        instructionTextArea.setEditable(false);
        instructionTextArea.setColumns(20);
        instructionTextArea.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        instructionTextArea.setRows(5);
        instructionTextArea.setText("Description: \n- Separate complete loops with semicolons,  singele elements of the loops with commas\n- Separate diagonalboard-connections with commas.The connectio of the first doublescrambler comes to the first position.\n- Difference to te beginning and the diagonalboard don't work together!");
        instructionTextArea.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        instructionTextArea.setEnabled(false);
        instructionTextArea.setMinimumSize(new java.awt.Dimension(556, 74));
        instructionTextArea.setRequestFocusEnabled(false);
        instructionScrollPane.setViewportView(instructionTextArea);

        loadLastInputButton.setText("Load  last input");
        loadLastInputButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        loadLastInputButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadLastInputButtonActionPerformed(evt);
            }
        });

        differenceToBeginningField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                differenceToBeginningFieldActionPerformed(evt);
            }
        });

        jLabel3.setText("Difference to the beginning of the text:");
        jLabel3.setFocusable(false);

        javax.swing.GroupLayout contentPanelLayout = new javax.swing.GroupLayout(contentPanel);
        contentPanel.setLayout(contentPanelLayout);
        contentPanelLayout.setHorizontalGroup(
            contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(contentPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(backButton))
                    .addComponent(solutionTableScrollPane, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(instructionScrollPane, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, contentPanelLayout.createSequentialGroup()
                        .addGroup(contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(contentPanelLayout.createSequentialGroup()
                                .addGroup(contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(connectionsField))
                                    .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(loopField, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel11)
                                        .addGap(18, 18, 18)
                                        .addComponent(charactersRadioButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(numbersRadioButton)))
                                .addGroup(contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(nrOfSolutionsField))
                                    .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addGap(4, 4, 4)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(differenceToBeginningField))
                                    .addGroup(contentPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(loadLastInputButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cableSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel9)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(wireSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(0, 31, Short.MAX_VALUE)))
                .addGap(20, 20, 20))
        );
        contentPanelLayout.setVerticalGroup(
            contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contentPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(instructionScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(loopField)
                    .addComponent(jLabel3)
                    .addComponent(differenceToBeginningField))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(connectionsField)
                    .addComponent(jLabel8)
                    .addComponent(cableSpinner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(wireSpinner)
                    .addComponent(loadLastInputButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startButton)
                    .addComponent(jLabel11)
                    .addComponent(charactersRadioButton)
                    .addComponent(numbersRadioButton)
                    .addComponent(nrOfSolutionsField)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addComponent(solutionTableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(backButton)
                .addGap(74, 74, 74))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addComponent(contentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addComponent(contentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void differenceToBeginningFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_differenceToBeginningFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_differenceToBeginningFieldActionPerformed

    private void loadLastInputButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadLastInputButtonActionPerformed
        loadLastInput();
    }//GEN-LAST:event_loadLastInputButtonActionPerformed

    private void connectionsFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectionsFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_connectionsFieldActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        startButton.setSelected(true);
        resetColors();

        // <editor-fold defaultstate="collapsed" desc="Difference to Beginning is read out and saved as int">
        int differenceToBeginning = 0;
        String diffToBeginning = differenceToBeginningField.getText().replaceAll(" ", "");
        boolean mistakeInDifferenceToBeginningField = containsNOTOnly(diffToBeginning, NUMBERS);

        if (diffToBeginning.equals("")) {
            differenceToBeginning = 0;
        } else if (!mistakeInDifferenceToBeginningField) {
            differenceToBeginning = Integer.parseInt(diffToBeginning);
            if (differenceToBeginning >= MAXWHEELORDERNR) {
                mistakeInDifferenceToBeginningField = true;
            }
        }
        // </editor-fold> 
        
        // <editor-fold defaultstate="collapsed" desc="Loops are read and saved in a Array">
        ArrayList<ArrayList<Integer>> loops = new ArrayList<>();
        String[] splitLoops = loopField.getText().replaceAll(" ", "").split(";");
        boolean mistakeInLoopField = containsNOTOnly(loopField.getText(), NUMBERS + ",; ");

        if (!(splitLoops.length == 1 && splitLoops[0].equals("")) && !mistakeInLoopField) {
            for (String splitedLoops : splitLoops) {
                String[] splitLoop = splitedLoops.split(",");
                ArrayList<Integer> loop = new ArrayList<>();
                for (String splitLoop1 : splitLoop) {
                    if (!splitLoop1.isEmpty()) {
                        loop.add(Integer.parseInt(splitLoop1.trim()));
                    }
                }
                if (!loop.isEmpty()) {
                    loops.add(loop);
                }
            }
        }
        // </editor-fold> 
        
        // <editor-fold defaultstate="collapsed" desc="Connections are read and saved in a Array">
        ArrayList<String> connections = new ArrayList<>();
        String[] splitConnections = connectionsField.getText().replaceAll(" ", "").split(",");
        boolean mistakeInConnectionsField = false;

        for (String splittedConnection : splitConnections) {
            String actualSplittedConnection = splittedConnection.trim().toUpperCase();
            if (actualSplittedConnection.length() != 2 || containsNOTOnly(actualSplittedConnection, ALPHABET + ", ")) {
                mistakeInConnectionsField = true;
            } else if (!actualSplittedConnection.isEmpty()) {
                connections.add(actualSplittedConnection);
            }
        }

        if (connectionsField.getText().isEmpty()) {
            connections.clear();
            mistakeInConnectionsField = false;
        }
        // </editor-fold> 
        
        // <editor-fold defaultstate="collapsed" desc="Smallest and biggest number in all loops are determined">
        int min = 0;
        int max = 0;
        if (!loops.isEmpty()) {
            min = loops.get(0).get(0);
            max = min;
            for (ArrayList<Integer> loop : loops) {
                for (Integer loopPart : loop) {
                    if (loopPart < min) {
                        min = loopPart;
                    } else if (loopPart > max) {
                        max = loopPart;
                    }
                }
            }
        }
        // </editor-fold> 
        
        // <editor-fold defaultstate="collapsed" desc="Error messages, warnings and run Turingbombe">
        // <editor-fold defaultstate="collapsed" desc="Error Messages">
        if (loops.isEmpty() && !mistakeInLoopField) {
            loopField.setBackground(Color.RED);
            JOptionPane.showMessageDialog(this, "Please add at least one loop!", "No loop added", JOptionPane.ERROR_MESSAGE);
        } else if(max>=DEFAULTCHARNUMBER){
            loopField.setBackground(Color.RED);
            JOptionPane.showMessageDialog(this, "The numbers in the loops field is bigger than 25 so it's not supported.", "Too big number in loops field", JOptionPane.ERROR_MESSAGE);
        } else if (!(max - min < 25) && !loops.isEmpty()) {
            loopField.setBackground(Color.RED);
            JOptionPane.showMessageDialog(this, "The difference of the loops is too big so that there won't be a solution", "Too big difference in the loops", JOptionPane.ERROR_MESSAGE);
        } else if (mistakeInLoopField) {
            loopField.setBackground(Color.RED);
            JOptionPane.showMessageDialog(this, "There is a mistake in the Loop-Field! Please make sure that it contains only numbers, kommas and semikolons.", "Mistake in the Connections-Field", JOptionPane.ERROR_MESSAGE);
        } else if (mistakeInDifferenceToBeginningField) {
            differenceToBeginningField.setBackground(Color.RED);
            JOptionPane.showMessageDialog(this, "There is a mistake in the Difference-Field! Please make sure that it contains only numbers and that it is not bigger than " + MAXWHEELORDERNR + ".", "Mistake in the Difference-Field", JOptionPane.ERROR_MESSAGE);
        } else if (mistakeInConnectionsField) {
            connectionsField.setBackground(Color.RED);
            JOptionPane.showMessageDialog(this, "There is a mistake in the Connections-Field! Please make sure that it contains only characters and kommas and that the size of each connection is two.", "Mistake in the Connections-Field", JOptionPane.ERROR_MESSAGE);
        } else if (connections.size()>DEFAULTCHARNUMBER-1) {
            connectionsField.setBackground(Color.RED);
            JOptionPane.showMessageDialog(this, "There are too many Connections. Max number can be 26", "Too many connections", JOptionPane.ERROR_MESSAGE);
        } else {// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Run Turing-Bombe and warnings">
            saveLastInput();

            boolean tooBigDifferenceWarningAccepted = true;
            boolean tooIntensiveWarningAccepted = true;
            boolean diagonalboardDifferenceWarningAccepted = true;

            if (max - min > 12) {
                tooBigDifferenceWarningAccepted = tooBigDifferenceWarning();
            } 
            if (loops.size() < 3 && !connections.isEmpty()) {
                tooIntensiveWarningAccepted = tooIntensiveWarning();
            }
            if ((differenceToBeginning !=0 )&& !connections.isEmpty()) {
                diagonalboardDifferenceWarningAccepted = diagonalboardDifferenceWarning();
            }

            if (tooBigDifferenceWarningAccepted && tooIntensiveWarningAccepted && diagonalboardDifferenceWarningAccepted) {
                TuringBomb tb = new TuringBomb(loops, connections, ((String) cableSpinner.getValue()).trim(), ((String) wireSpinner.getValue()).trim());
                outputs = tb.findAllPossibilities(differenceToBeginning, !connections.isEmpty()); //+/- min testen
                newTablemodel();
                nrOfSolutionsField.setText("" + outputs.size());
            }
            // </editor-fold>
        }
        // </editor-fold>
        
        startButton.setSelected(false);
    }//GEN-LAST:event_startButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        this.setVisible(false);
        jtb.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_backButtonActionPerformed

    private void loopFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loopFieldActionPerformed

    }//GEN-LAST:event_loopFieldActionPerformed

    private void cableSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_cableSpinnerStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cableSpinnerStateChanged

    private void numbersRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numbersRadioButtonActionPerformed
        charactersRadioButton.setSelected(false);
        newTablemodel();
    }//GEN-LAST:event_numbersRadioButtonActionPerformed

    private void nrOfSolutionsFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nrOfSolutionsFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nrOfSolutionsFieldActionPerformed

    private void charactersRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_charactersRadioButtonActionPerformed
        numbersRadioButton.setSelected(false);
        newTablemodel();
    }//GEN-LAST:event_charactersRadioButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JSpinner cableSpinner;
    private javax.swing.JRadioButton charactersRadioButton;
    private javax.swing.JTextField connectionsField;
    private javax.swing.JPanel contentPanel;
    private javax.swing.JTextField differenceToBeginningField;
    private javax.swing.JScrollPane instructionScrollPane;
    private javax.swing.JTextArea instructionTextArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton loadLastInputButton;
    private javax.swing.JTextField loopField;
    private javax.swing.JTextField nrOfSolutionsField;
    private javax.swing.JRadioButton numbersRadioButton;
    private javax.swing.JTable solutionTable;
    private javax.swing.JScrollPane solutionTableScrollPane;
    private javax.swing.JButton startButton;
    private javax.swing.JSpinner wireSpinner;
    // End of variables declaration//GEN-END:variables

    private boolean tooBigDifferenceWarning() {
        Object[] options = {"Continue", "Chancel"};
        return JOptionPane.OK_OPTION == JOptionPane.showOptionDialog(null, "The difference between the elemnets of the loops is to big. If you continue the chance of finding the right solution is below 50%!", "Difference between the elements of the Loops to big", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    }

    private boolean tooIntensiveWarning() {
        Object[] options = {"Continue", "Chancel"};
        return JOptionPane.OK_OPTION == JOptionPane.showOptionDialog(null, "To use the diagonalboard in this combination may be to CPU-intensive. It is recommanded to use at least three loops.", "Too CPU-intensive", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    }
    
    private boolean diagonalboardDifferenceWarning() {
        Object[] options = {"Continue", "Chancel"};
        return JOptionPane.OK_OPTION == JOptionPane.showOptionDialog(null, "Difference to te beginning and the diagonalboard don't work together! If you continue, the diagonalboard will give shifted/wrong outputs.", "Difference to te beginning and the diagonalboard don't work together!", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    }

    private void newTablemodel() {
        solutionTable.setModel(new turing.bomb.GUI.TableModel(outputs, numbersRadioButton.isSelected()));
        for (int i = 0; i < 8; i++) {
            TableColumn col = solutionTable.getColumnModel().getColumn(i);
            col.setPreferredWidth(52);
        }
        TableColumn col = solutionTable.getColumnModel().getColumn(8);
        col.setPreferredWidth(375);
    }

    private void saveLastInput() {//saves last input (doesn't work propperly in this configuration, because the lastInputs can not be edited when saven in the project), so it creates a new file in the folder
        try {
            try (PrintWriter pw = new PrintWriter(new File(filename), "UTF-8")) {
                pw.println(loopField.getText());
                pw.println(connectionsField.getText());
                pw.println(differenceToBeginningField.getText());
            }
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
        }
    }

    private void loadLastInput() {//loads last input
        try {
            FileInputStream fin = new FileInputStream(filename);
            InputStreamReader isr = new InputStreamReader(fin, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            loopField.setText(br.readLine());
            connectionsField.setText(br.readLine());
            differenceToBeginningField.setText(br.readLine());
        } catch (Exception ex) {
        }
    }

    private void resetColors() {//resets the color of all elements
        this.getContentPane().setBackground(Color.black);
        contentPanel.setBackground(BACKGROUNDCOLOR);
        startButton.setBackground(BUTTONCOLOR);
        loadLastInputButton.setBackground(BUTTONCOLOR);
        backButton.setBackground(BUTTONCOLOR);
        nrOfSolutionsField.setBackground(BUTTONCOLOR);
        loopField.setBackground(BUTTONCOLOR);
        connectionsField.setBackground(BUTTONCOLOR);
        differenceToBeginningField.setBackground(BUTTONCOLOR);

        wireSpinner.getEditor().setBackground(BACKGROUNDCOLOR);
        cableSpinner.getEditor().setBackground(BACKGROUNDCOLOR);

        solutionTable.setSelectionBackground(Color.LIGHT_GRAY);
        solutionTable.setBackground(BUTTONCOLOR);
        solutionTable.setGridColor(BACKGROUNDCOLOR);
    }

    private boolean containsNOTOnly(String string, String characters) {//Tests weather the first string does not only contain elements of the second string
        for (int i = 0; i < string.length(); i++) {
            if (!characters.contains(string.substring(i, i + 1))) {
                return true;
            }
        }
        return false;
    }
}
