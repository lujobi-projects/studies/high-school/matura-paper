/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turing.bomb.Test;

import turing.bomb.RotorPart.Reader;

/**
 *
 * @author Luzian
 */
public class Test_Rotors {

    Reader reader = new Reader();
    int[] testList = new int[26];

    public Test_Rotors() {
        for (int i = 0; i < 5; i++) {
            printOutList(reader.getRotor(i + 1));
        }

        for (int i = 0; i < 3; i++) {
            printOutList(reader.getReflector(i + 1));
        }
    }

    private void printOutList(int[] list) {
        System.out.println();
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i] + "\t");
        }
    }

}
