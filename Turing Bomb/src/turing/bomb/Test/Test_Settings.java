package turing.bomb.Test;

import turing.bomb.RotorPart.WheelOrder;
import turing.bomb.RotorPart.Settings;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Luzian
 */
public class Test_Settings {

    Settings s = new Settings(new WheelOrder(1, 3, 5, 1));

    public Test_Settings() {
        System.out.println();
        for (int i = 0; i < 30; i++) {
            System.out.print(s.input(5, 18) + "\t");
            s.turnDS();
        }
        System.out.println();
    }

}
